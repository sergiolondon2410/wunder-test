POSSIBLE PERFORMANCE OPTIMIZATIONS?
The database performance can be better if stored procedures are used, especially in cases where the model must call some queries and validations. I think that if the app requires large frequent queries, maybe I would use some javascript third-party libraries like datatables which handle data in the client side.

THINGS I COULD DO BETTER
I like to do better validations because avoid some errors form data acquisition. Honestly, I could try to enhance the way to call the resources, like views or files and be more careful with the permissions of public and private folders.

WHY MVC?
I chose the MVC pattern because this project was simple and I do not need to think about its scalability. The app did not need optimizations in order to show a huge quantity of information, so I felt free to get focus in to show my experience as PHP and backend developer, this is the reason why do not use complex javascript libraries in order to give more data handling capacities to the views. 